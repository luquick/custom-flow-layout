package com.pyf.flowlayout.net;

import com.pyf.flowlayout.bean.HotKey;

import poetry.jianjia.Call;
import poetry.jianjia.http.GET;

/**
 * @author 裴云飞
 * @date 2021/5/16
 */

public interface Wan {

    @GET("/hotkey/json")
    Call<HotKey> getHotKey();
}
