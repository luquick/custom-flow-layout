package com.pyf.flowlayout;

/**
 * @author 裴云飞
 * @date 2021/5/16
 */

public class TextUtils {

    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }
}
