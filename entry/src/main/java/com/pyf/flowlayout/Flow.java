package com.pyf.flowlayout;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * @author 裴云飞
 * @date 2021/5/15
 */

public class Flow extends Component implements Component.EstimateSizeListener {
    public Flow(Context context) {
        this(context, null);
    }

    public Flow(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public Flow(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setEstimateSizeListener(this);
    }

    @Override
    public boolean onEstimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        // 得到宽度的测量模式
        int widthMode = EstimateSpec.getMode(widthEstimatedConfig);
        // 得到高度的测量模式
        int heightMode = EstimateSpec.getMode(heightEstimatedConfig);
        // 得到宽度的测量大小
        int width = EstimateSpec.getSize(widthEstimatedConfig);
        // 得到高度的测量大小
        int height = EstimateSpec.getSize(heightEstimatedConfig);
        switch (widthMode) {
            case EstimateSpec.PRECISE:
                LogUtils.info("yunfei宽度", "精确");
                break;
            case EstimateSpec.NOT_EXCEED:
                LogUtils.info("yunfei宽度", "不超过");
                break;
            case EstimateSpec.UNCONSTRAINT:
                LogUtils.info("yunfei宽度", "不受约束");
                break;
            default:
                LogUtils.info("yunfei宽度", "默认");
                break;
        }
        switch (heightMode) {
            case EstimateSpec.PRECISE:
                LogUtils.info("yunfei高度", "精确");
                break;
            case EstimateSpec.NOT_EXCEED:
                LogUtils.info("yunfei高度", "不超过");
                break;
            case EstimateSpec.UNCONSTRAINT:
                LogUtils.info("yunfei高度", "不受约束");
                break;
            default:
                LogUtils.info("yunfei高度", "默认");
                break;
        }
        return false;
    }
}
