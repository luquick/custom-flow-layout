package com.pyf.flowlayout.slice;

import com.pyf.flowlayout.FlowLayout;
import com.pyf.flowlayout.LogUtils;
import com.pyf.flowlayout.MyApplication;
import com.pyf.flowlayout.ResourceTable;
import com.pyf.flowlayout.TextUtils;
import com.pyf.flowlayout.bean.HotKey;
import com.pyf.flowlayout.bean.HotKey.Data;

import java.util.List;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import poetry.jianjia.Call;
import poetry.jianjia.Callback;
import poetry.jianjia.Response;

public class MainAbilitySlice extends AbilitySlice {

    private FlowLayout mFlowLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mFlowLayout = (FlowLayout) findComponentById(ResourceTable.Id_flow_layout);
        // 请求服务端的搜索热词
        MyApplication.getInstance().getWan().getHotKey().enqueue(new Callback<HotKey>() {
            @Override
            public void onResponse(Call<HotKey> call, Response<HotKey> response) {
                // 请求成功
                if (response.isSuccessful()) {
                    HotKey hotKey = response.body();
                    // 设置搜索热词
                    setHotKey(hotKey);
                }
            }

            @Override
            public void onFailure(Call<HotKey> call, Throwable throwable) {
                // 请求失败
                LogUtils.info("yunfei", throwable.getMessage());
            }
        });
    }

    /**
     * 设置搜索热词
     *
     * @param hotKey
     */
    private void setHotKey(HotKey hotKey) {
        if (hotKey == null || hotKey.data == null || hotKey.data.isEmpty()) {
            // 判空操作
            return;
        }
        List<Data> hotKeys = hotKey.data;
        for (Data data : hotKeys) {
            // 将布局文件转换成组件对象，并强转为Text组件
            Text text = (Text) LayoutScatter.getInstance(this).
                    parse(ResourceTable.Layout_item_text, null, false);
            if (data != null && !TextUtils.isEmpty(data.name)) {
                // 显示组件的内容
                text.setText(data.name);
                // 将组件添加到流式布局
                mFlowLayout.addComponent(text);
            }
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
