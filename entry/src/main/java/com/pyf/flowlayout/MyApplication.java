package com.pyf.flowlayout;

import com.pyf.flowlayout.net.Wan;

import ohos.aafwk.ability.AbilityPackage;
import poerty.jianjian.converter.gson.GsonConverterFactory;
import poetry.jianjia.JianJia;

public class MyApplication extends AbilityPackage {

    private static MyApplication application;
    private JianJia mJianJia;
    private Wan mWan;

    public static MyApplication getInstance() {
        return application;
    }

    @Override
    public void onInitialize() {
        super.onInitialize();
        application = this;
        // 创建蒹葭对象
        mJianJia = new JianJia.Builder()
                .baseUrl("https://www.wanandroid.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // 创建接口的实例对象
        mWan = mJianJia.create(Wan.class);
    }

    /**
     * 获取蒹葭对象
     *
     * @return 蒹葭对象
     */
    public JianJia getJianJia() {
        return mJianJia;
    }

    /**
     * 获取接口的实例对象
     *
     * @return
     */
    public Wan getWan() {
        return mWan;
    }
}
