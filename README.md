### 一、介绍
在鸿蒙系统上自定义流式布局
### 二、效果图
![流式布局](./screenshot/流式布局.png)

效果图里面的内容是从服务器获取的，我们使用蒹葭网络库来请求服务器，
蒹葭是鸿蒙系统上一款网络请求框架，本质上是从`retrofit`移植过来的，
蒹葭的用法跟`retrofit`是一样，如果不熟悉蒹葭的用法，
可以阅读[鸿蒙系统网络请求框架—蒹葭](https://www.jianshu.com/p/329552e71442) 这篇文章。
在示例代码中，我们会访问[搜索热词](https://www.wanandroid.com//hotkey/json) 
这个接口，这个接口用于获取搜索热词。
### 三、示例代码讲解
3、1 在配置文件中添加访问网络的权限
```
"ohos.permission.INTERNET"
```
3、2 打开entry目录下的build.gradle文件中，在build.gradle文件中的dependencies闭包下添加下面的依赖。
```
// 蒹葭的核心代码
implementation 'io.gitee.zhongte:jianjia:1.0.1'
// 数据转换器，数据转换器使用gson来帮我们解析json，不需要我们手动解析json
implementation 'io.gitee.zhongte:converter-gson:1.0.1'
implementation "com.google.code.gson:gson:2.8.2"
```
3、3 在布局文件添加流式布局
```
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="vertical">

    <com.pyf.flowlayout.FlowLayout
        ohos:id="$+id:flow_layout"
        ohos:height="match_content"
        ohos:width="match_parent"/>

</DirectionalLayout>
``` 
3、4 在布局文件中创建流式布局的子组件
```
<?xml version="1.0" encoding="utf-8"?>
<Text
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:background_element="$graphic:background_ability_main"
    ohos:height="match_content"
    ohos:margin="10vp"
    ohos:padding="10vp"
    ohos:text_alignment="center"
    ohos:text_color="#ffffff"
    ohos:text_size="15fp"
    ohos:width="match_content">

</Text>
```
3、5 创建接口
```
/**
 * @author 裴云飞
 * @date 2021/5/16
 */

public interface Wan {

    @GET("/hotkey/json")
    Call<HotKey> getHotKey();
}
```
3、6 在`AbilityPackage`中创建蒹葭对象，并创建接口的实例对象 
```
public class MyApplication extends AbilityPackage {

    private static MyApplication application;
    private JianJia mJianJia;
    private Wan mWan;

    public static MyApplication getInstance() {
        return application;
    }

    @Override
    public void onInitialize() {
        super.onInitialize();
        application = this;
        // 创建蒹葭对象
        mJianJia = new JianJia.Builder()
                .baseUrl("https://www.wanandroid.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // 创建接口的实例对象
        mWan = mJianJia.create(Wan.class);
    }

    /**
     * 获取蒹葭对象
     *
     * @return 蒹葭对象
     */
    public JianJia getJianJia() {
        return mJianJia;
    }

    /**
     * 获取接口的实例对象
     *
     * @return
     */
    public Wan getWan() {
        return mWan;
    }
}
```
3、7 在`MainAbilitySlice`里面使用蒹葭访问服务器，将搜索热词显示到流式布局里面
```
public class MainAbilitySlice extends AbilitySlice {

    private FlowLayout mFlowLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mFlowLayout = (FlowLayout) findComponentById(ResourceTable.Id_flow_layout);
        // 请求服务端的搜索热词
        MyApplication.getInstance().getWan().getHotKey().enqueue(new Callback<HotKey>() {
            @Override
            public void onResponse(Call<HotKey> call, Response<HotKey> response) {
                // 请求成功
                if (response.isSuccessful()) {
                    HotKey hotKey = response.body();
                    // 设置搜索热词
                    setHotKey(hotKey);
                }
            }

            @Override
            public void onFailure(Call<HotKey> call, Throwable throwable) {
                // 请求失败
                LogUtils.info("yunfei", throwable.getMessage());
            }
        });
    }

    /**
     * 设置搜索热词
     *
     * @param hotKey
     */
    private void setHotKey(HotKey hotKey) {
        if (hotKey == null || hotKey.data == null || hotKey.data.isEmpty()) {
            // 判空操作
            return;
        }
        List<Data> hotKeys = hotKey.data;
        for (Data data : hotKeys) {
            // 将布局文件转换成组件对象，并强转为Text组件
            Text text = (Text) LayoutScatter.getInstance(this).
                    parse(ResourceTable.Layout_item_text, null, false);
            if (data != null && !TextUtils.isEmpty(data.name)) {
                // 显示组件的内容
                text.setText(data.name);
                // 将组件添加到流式布局
                mFlowLayout.addComponent(text);
            }
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
```
更多代码讲解，请查看[博客](https://www.jianshu.com/p/d9c2e1b4790a) ，
博客以自定义流式布局为例，非常详细的介绍了
自定义布局的各方面知识，非常适合鸿蒙的初学者。
### 四、感谢
感谢[玩Android](https://wanandroid.com/blog/show/2) 提供的开放接口
